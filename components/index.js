// @flow

import React, { Fragment, type Node } from 'react';

export const Emphasis = ({ children }: { children: Node }) => (
	<Fragment>
		<span>{children}</span>
		<style jsx>{`
			span {
				font-size: 1.1em;
				color: #ff8882;
			}
		`}</style>
	</Fragment>
);

export const Deemphasis = ({ children }: { children: Node }) => (
	<Fragment>
		<span>{children}</span>
		<style jsx>{`
			span {
				font-size: 0.9em;
			}
		`}</style>
	</Fragment>
);

const textStyle = `
	color: #e1be9a;
	font-family: "Futura", "Helvetia Neue", arial, sans-serif;
	-webkit-font-smoothing: antialiased;
`;

export const Paragraph = ({ children }: { children: Node }) => (
	<p>
		{children}
		<style jsx>{`
			p {
				${textStyle}
			}
		`}</style>
	</p>
);

export const Anchor = ({
	href,
	children,
}: {
	href: string,
	children: Node,
}) => (
	<a href={href}>
		{children}
		<style jsx>{`
			a:link {
				color: hotpink;
			}
			a:visited {
				color: hotpink;
			}
		`}</style>
	</a>
);

export const CallToAction = ({ href }: { href: string }) => (
	<a href={href}>
		<span>Sign Up</span>
		<style jsx>{`
			a {
				border: 3px #e1be9a solid;
				border-radius: 3px;
				padding: 2px 20px;
				text-transform: uppercase;
				outline: none;
				background: #a570ec;
				width: 100%;
				box-sizing: content-box;
				font-family: 'Futura', 'Helvetia Neue', arial, sans-serif;
				cursor: pointer;
				height: 40px;
				display: flex;
				align-items: center;
				justify-content: center;
				outline: none;
				color: inherit;
				text-decoration: inherit;
			}

			a:hover {
				background: #c38bff;
			}

			@media (max-width: 769px) {
				a {
					padding: 2px 0;
				}
			}
		`}</style>
	</a>
);

export const Header = ({ children }: { children: Node }) => (
	<div>
		<div className="header-wrapper">
			<div className="header-dash dash-left"></div>
			<h1>{children}</h1>
			<div className="header-dash dash-right"></div>
		</div>
		<style jsx>{`
			h1 {
				${textStyle}
				text-align: center;
				font-family: 'Pirulen', sans-serif;
				font-size: 2em;
			}
			.header-wrapper {
				display: flex;
				justify-content: space-between;
			}

			@media (max-width: 769px) {
				.header-wrapper h1,
				.header-wrapper h2,
				.header-wrapper h3 {
					width: 75%;
				}

				.header-wrapper h1 {
					font-size: 1.5em;
				}

				.header-wrapper h2 {
					font-size: 1.25em;
				}
			}

			.header-dash {
				flex: 1;
				background-repeat: no-repeat;
			}

			.dash-right {
				background-image: url('static/text-accent-right.png');
				background-position: top 50% left 30%;
				background-size: 200px;
			}

			@media (max-width: 769px) {
				.dash-right {
					background-position: top 50% left -3%;
				}
			}

			.dash-left {
				background-image: url('static/text-accent-left.png');
				background-position: top 50% right 30%;
				background-size: 200px;
			}

			@media (max-width: 769px) {
				.dash-left {
					background-position: top 50% right -3%;
				}
			}
		`}</style>
	</div>
);
