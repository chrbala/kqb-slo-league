// @flow

import React, { Fragment, useEffect } from 'react';
import Head from 'next/head';
// $FlowFixMe
import banner from '../static/banner.png?sizes[]=1024,sizes[]=320,sizes[]=480,sizes[]=768,sizes[]=100,sizes[]=1800';
import {
	Emphasis,
	Deemphasis,
	Paragraph,
	Anchor,
	CallToAction,
	Header,
} from '../components';
import '../util/globals';
import ReactGA from 'react-ga';

export const Banner = () => (
	<Fragment>
		<img alt="" srcSet={banner.srcSet} src={banner.src} />
		<style jsx>{`
			img {
				margin: 0;
				display: block;
				width: 100%;
			}
		`}</style>
	</Fragment>
);

export const Video = () => (
	<div className="video-content">
		<div className="video-wrapper">
			<iframe
				className="video-frame"
				src="https://www.youtube.com/embed/UkrmYro1Hys?rel=0&amp;controls=0&amp;showinfo=0"
				frameBorder="0"
				allow="autoplay; encrypted-media"
				allowFullScreen=""
			></iframe>
		</div>
		<style jsx>{`
			.video-content {
				width: 65%;
				margin: 0 auto;
				position: absolute;
				top: 50px;
				left: 0;
				right: 0;
				box-shadow: 0 0 20px #000;
			}
			.video-wrapper {
				position: relative;
				height: 0;
				overflow: hidden;
				padding-bottom: 56.25%;
			}
			.video-frame {
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
			}
		`}</style>
	</div>
);

export const Hero = () => (
	<div className="container">
		<Banner />
		<Video />
		<style jsx>{`
			.container {
				position: relative;
				padding-bottom: 100px;
			}
		`}</style>
	</div>
);

const Content = ({ children }) => (
	<div className="content-wrapper">
		<div className="splatter-left"></div>

		<div className="content">{children}</div>

		<div className="splatter-right"></div>
		<style jsx>{`
			.content-wrapper {
				margin: 0 auto 1em;
				display: flex;
				justify-content: center;
			}

			.splatter-right,
			.splatter-left {
				width: 25%;
				background-size: 80% auto;
				background-repeat: no-repeat;
				background-position: top 50% left 50%;
				z-index: -1;
			}

			@media (max-width: 900px) {
				.splatter-right,
				.splatter-left {
					display: none;
				}
			}

			.splatter-right {
				background-image: url('/static/splatter-right.png');
				background-size: auto 100%;
				margin-left: 15px;
			}

			.splatter-left {
				background-image: url('/static/splatter-left.png');
				background-size: auto 100%;
				margin-right: 15px;
			}

			.content {
				width: 75%;
				max-width: 600px;
			}

			@media (max-width: 769px) {
				.content {
					width: 95%;
				}
			}
		`}</style>
	</div>
);

const domain = 'https://kqb-league.com';
const title = 'Killer Queen Black League';
const shareImage = `${domain}/static/social.png`;
const shareDescription =
	'Killer Queen Black League is a group of people in the San Luis Obispo area who play Killer Queen Black with each other both in person and online.';

const OpenGraph = () => (
	<Fragment>
		<meta property="og:type" content="website" />
		<meta property="og:title" content={title} />
		<meta property="og:description" content={shareDescription} />
		<meta property="og:image" content={shareImage} />
		<meta property="og:image:secure_url" content={shareImage} />
		<meta property="og:image:alt" content="Killer Queen Black League Logo" />
		<meta property="og:image:width" content="1902" />
		<meta property="og:image:height" content="1920" />
		<meta property="og:title" content={title} />
		<meta property="og:url" content={domain} />
	</Fragment>
);

const TwitterCards = () => (
	<Fragment>
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:title" content={title} />
		<meta name="twitter:description" content={shareDescription} />
		<meta name="twitter:image:src" content={shareImage}></meta>
	</Fragment>
);

export default () => {
	useEffect(() => {
		ReactGA.pageview(window.location.pathname + window.location.search);
	}, []);

	return (
		<div>
			<Head>
				<title>{title}</title>
				<link rel="icon" href="/static/favicon.ico"></link>
				<meta name="theme-color" content="#a570ec" />
				<meta name="description" content={shareDescription} />
				<meta
					name="google-site-verification"
					content="94zgviqsTPBOVPZU32LJBzigAPmA71UmlNvA9PwDwzE"
				/>
				<meta name="msvalidate.01" content="E0E54C74DFA7DCCA8D15228C64999C06" />
				<OpenGraph />
				<TwitterCards />
			</Head>
			<style jsx global>{`
				body {
					background-color: #03000a;
					padding: 0;
					margin: 0;
				}

				@font-face {
					font-family: 'Pirulen';
					font-weight: bold;
					src: url('/static/pirulen_bd.woff') format('woff');
					src: url('/static/pirulen_bd.otf') format('opentype');
				}

				@font-face {
					font-family: 'Futura';
					font-weight: normal;
					src: url('/static/futura_md.otf') format('opentype');
				}
			`}</style>
			<Hero />
			<Header>Killer Queen Black League</Header>
			<Content>
				<Paragraph>
					<Emphasis>Killer Queen Black</Emphasis> is an intense multiplayer
					action/strategy platformer for up to eight players. Hop on the snail,
					hoard berries, or wipe out the enemy’s queen to claim victory.
				</Paragraph>

				<Paragraph>
					<Emphasis>Killer Queen Black League</Emphasis> is a group of people in
					the San Luis Obispo area who play Killer Queen Black with each other
					both in person and online. All skill levels are welcome: we'll help
					you get started if you're new to playing!
				</Paragraph>

				<Paragraph>
					If what you've read so far sounds good, you should sign up!
				</Paragraph>

				<CallToAction href="https://discord.gg/gwuWC2k">Sign up</CallToAction>

				<Paragraph>
					<Deemphasis>
						For more information about the game, take a look at the{' '}
						<Anchor href="http://www.killerqueenblack.com">
							official Killer Queen Black site
						</Anchor>
					</Deemphasis>
					.
				</Paragraph>
			</Content>
		</div>
	);
};
