#!/usr/bin/env node

// @flow

const { readFileSync, writeFileSync, existsSync } = require('fs');

const gitIgnorePath = './.gitignore';

// Now deployments seem to drop the .gitignore file, so skip this there
if (existsSync(gitIgnorePath)) {
	const gitIgnore = readFileSync(gitIgnorePath, 'utf8');
	const out = [gitIgnore, 'flow-typed'].join('\n');

	['.eslintignore', '.prettierignore'].forEach(ignoreFile =>
		writeFileSync(ignoreFile, out)
	);
}
