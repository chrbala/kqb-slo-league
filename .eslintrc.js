const createLinter = require('chrbala-linter');
const linter = createLinter({ modules: ['eslint', 'react', 'flow'] });
delete linter.rules['max-params'];
delete linter.rules['indent'];
delete linter.rules['complexity'];
module.exports = linter;
